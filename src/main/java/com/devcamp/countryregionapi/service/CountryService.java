package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionService;

    Country vietnam = new Country("VN", "Viet Nam");
    Country us = new Country("US", "My");
    Country russia = new Country("RUS", "Nga");

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = new ArrayList<>();

        vietnam.setRegions(regionService.getRegionVietNam());
        us.setRegions(regionService.getRegionUS());
        russia.setRegions(regionService.getRegionRussia());

        allCountry.add(vietnam);
        allCountry.add(us);
        allCountry.add(russia);

        return allCountry;
    }
}
