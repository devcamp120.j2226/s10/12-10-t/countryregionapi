package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Region;

@Service
public class RegionService {
    Region hanoi = new Region("HN", "Ha Noi");
    Region tpHoChiMinh = new Region("HCM", "Thanh pho Ho Chi Minh");
    Region danang = new Region("DN", "Da Nang");

    Region newyorks = new Region("NY", "New Yorks");
    Region florida = new Region("FLO", "Florida");
    Region texas = new Region("TX", "Texas");

    Region moscow = new Region("MC", "Moscow");
    Region kaluga = new Region("KL", "Kaluga");
    Region saintpeterburg = new Region("SP", "Saint Peterburg");

    public ArrayList<Region> getRegionVietNam() {
        ArrayList<Region> regionsVietNam = new ArrayList<>();

        regionsVietNam.add(hanoi);
        regionsVietNam.add(tpHoChiMinh);
        regionsVietNam.add(danang);

        return regionsVietNam;
    }

    public ArrayList<Region> getRegionUS() {
        ArrayList<Region> regionsUS = new ArrayList<>();

        regionsUS.add(newyorks);
        regionsUS.add(florida);
        regionsUS.add(texas);

        return regionsUS;
    }

    public ArrayList<Region> getRegionRussia() {
        ArrayList<Region> regionsRussia = new ArrayList<>();

        regionsRussia.add(moscow);
        regionsRussia.add(kaluga);
        regionsRussia.add(saintpeterburg);

        return regionsRussia;
    }

    // C2: Dồn hết việc cho service
    public Region filterRegions(String regionCode) {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(hanoi);
        regions.add(tpHoChiMinh);
        regions.add(danang);
        regions.add(newyorks);
        regions.add(florida);
        regions.add(texas);
        regions.add(moscow);
        regions.add(kaluga);
        regions.add(saintpeterburg);

        Region findRegion = new Region();

        for (Region regionElement : regions) {
            if(regionElement.getRegionCode().equals(regionCode)) {
                findRegion = regionElement;
            }
        }

        return findRegion;
    }
}
